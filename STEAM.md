# Steam

## Proton Tricks

Wine doesn't always support all Windows libraries and some games don't run or
have glitches because of that. One workaround is to install Windows libraries
and that can be done with winetricks. In case of Steam, however, it's easier to
user protontricks, because it will specifically change the prefix of a
particular steam game by ID, instead of a generic prefix.

### Installation

In order to install protontricks:

```sh
git clone https://aur.archlinux.org/protontricks.git
cd protontricks
makepkg -si
```

Or use a AUR helper like like paru:

```sh
paru -Syu protontricks
```

### Usage

The easiest way is to first list all games:

```sh
protrontricks -l
```

Or search for a specific game:

```sh
protontricks -s GAME_NAME
```

With the APPID you use any wine command. For example, to see what's installed:

```sh
protontricks <APPID> list-installed
```

Steam and ProtonGE sometimes install libraries or fonts using protontricks,
listing the installed libraries is good for debugging purposes.

## Games

This section is for specific game issues.

### Epic Battle Fantasy 4

It has a conflict with Windows libraries.

Run the following script to fix:

```bash
#!/bin/bash

# Set to exit on any error
set -e

DEF_STEAM_APPS="$HOME/.local/share/Steam/steamapps"
COMPAT_DATA_PATH="$STEAM_APPS/compatdata/265610"
WINDOWS_PATH="$COMPAT_DATA_PATH/pfx/drive_c/windows"
SYSTEM32_PATH="$WINDOWS_PATH/system32"
SYSTEM64_PATH="$WINDOWS_PATH/syswow64"

if [[ -z $STEAM_APPS ]]; then
        printf "STEAM_APPS environment variable not set\n"
        printf "Setting to the default %s" "$DEF_STEAM_APPS"
        STEAM_APPS="$DEF_STEAM_APPS"
        exit 1
fi

echo "Removing 32-bit kernel and user libraries"
mv "$SYSTEM32_PATH/kernel32.dll" "$SYSTEM32_PATH/kernel32.dll.bck"
mv "$SYSTEM32_PATH/user32.dll" "$SYSTEM32_PATH/user32.dll.bck"

echo "Removing 64-bit kernel and user libraries"
mv "$SYSTEM64_PATH/kernel32.dll" "$SYSTEM64_PATH/kernel32.dll.bck"
mv "$SYSTEM64_PATH/user32.dll" "$SYSTEM64_PATH/user32.dll.bck"
```

[winetricks]: https://wiki.winehq.org/Winetricks
[protontricks]: https://github.com/Matoking/protontricks
