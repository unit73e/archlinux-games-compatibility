# Gamepad

This document is based on [gamepad](https://wiki.archlinux.org/title/Gamepad).

## Playstation 4

Works out of the box with USB.

How to test:

```
# Joystick API
$jstest /dev/input/by-id/usb-Sony_Computer_Entertainment_Wireless_Controller-joystick

# EvDev
evtest /dev/input/by-id/usb-Sony_Computer_Entertainment_Wireless_Controller-event-if00

# Wine
wine control joy.cpl
```

Wireless needs a bit more effort:

```
$ pacman -S bluez bluez-utils
$ systemctl start bluetooth
$ bluetoothctl
> power on
> scan on
# Press Playstation and Share simultaneously
# Wait for "Wireless Controller" to show up
> trust <MAC>
> pair <MAC>
# The light should be blue
```

To connect the next time press the Playstation button. Testing should be
identical, but there's a catch, wine will most likely not work.

To fix that problem you can mimic the XBox Controller:

```
$ paru -S sc-controller
$ sc-controller

# Go to Menu -> Settings -> Controllers
# Press "Register New Controller"
# Choose "Wireless Controller"
# On labels choose the Playstation scheme
# On advanced options choose "evtest" because USB HID won't work
# Press all buttons and move all analogs
# Press save
```

There are 3 profiles by default:

- Desktop - a mix of keyboard and mouse mapping
- XBox Controller with High Precision Camera - Xbox mapping but the right
  analog is a trackball and Playstation button opens a menu
- XBox Controller - Xbox mapping and Playstation button opens a menu

The best option is a modified XBox Controller without the special menu:

```
# Pick "XBox Controller"
# Press the Playstation button
# Press "Menu"
# Press "Key or Button"
# Press the XBox Button
# Press OK
# Press Save
```

As an added bonus, you will also have an XInput controller:

```
wine control joy.cpl
```
