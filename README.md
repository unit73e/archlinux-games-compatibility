# Arch Linux games compatibility

This page documents how to run games, fix issues, and optimize games in Arch
Linux.

<!-- mtoc-start -->

* [General](#general)
  * [Useful Software](#useful-software)
  * [Recommended Setup](#recommended-setup)
  * [Japanese Games](#japanese-games)
  * [Video Issues](#video-issues)
  * [MIDI](#midi)
* [Game Engines](#game-engines)
* [Steam](#steam)

<!-- mtoc-end -->

## General

### Useful Software

To play Windows games:

- `wine` to play Windows games.
- `lutris` a manager for emulators, stores and wine.
- `protonplus` allows to install custom Proton/Wine
- `gamescope` to fix resolution, add FSR, or limit frame-rate.
- `steam` to play steam games.

### Recommended Setup

All Windows games need `wine`:

```
# Enable multilib in /etc/pacman.conf
[multilib]
Include = /etc/pacman.d/mirrorlist

# Install wine
pacman -Syu wine wine-gecko wine-mono

# Winetricks is recommended
pacman -S winetricks
```

For local games a custom wine version usually works better:

```
# Install Lutris
pacman -S lutris

# Gamemode and gamescope are recommended
pacman -S gamemode lib32-gamemode gamescope

# Install Protonplus
paru -S protonplus

# Start Protonplus
protonplus

# Select Lutris
# Install a Wine version by Kron4ek
# Install DXVK
# Install VKD3D
```

For Steam games:

```
pacman -S steam
```

Steam default Proton is usually good enough but for some games ProtonGE works
better:

```
protonplus

# Select Steam
# Select the latest ProtonGE

# Start Steam
steam

# To always use ProtonGE by default (not recommended):
#  Steam -> Settings
#  Compatibility -> Run other titles with
#  Select the latest ProtonGE
#
# To use ProtonGE in a particular game:
#  Library -> Right Click -> Properties
#  Compatibility -> Check "Force Steam Play compatibility tool"
#  Select latest ProtonGE
```

### Japanese Games

Japanese games most likely require changing locale:

- Uncomment `ja_JP.UTF-8 UTF-8` in `/etc/locale.gen`.
- Run `locale-gen`.
- Check with `locale -a` if Japanese is present.

These fonts are usually enough:

- `adobe-source-han-sans-jp-fonts`
- `adobe-source-han-serif-jp-fonts`

Wine `fakejapanese` uses the fonts above, but there are other fonts as well.

More information [here](https://wiki.archlinux.org/title/Localization/Japanese).

Run the game with:

```bash
LANG=ja_JP.UTF-8 wine <exec>
```

If fonts are still not showing up run:

```bash
winetricks fonts corefonts cjkfonts
```

Which will install core and Chinese, Japanese and Korean fonts in your wine
prefix.

### Video Issues

Wine uses Gstreamer to play videos. Currently these are the official packages
needed:

```sh
# Gstreamer application
gstreamer

# Hardware acceleration
gstreamer-vaapi

# Plugins
gst-libav
gst-plugin-pipewire
gst-plugins-bad
gst-plugins-bad-libs
gst-plugins-base
gst-plugins-base-libs
gst-plugins-good
gst-plugins-ugly

# For 32-bit games
lib32-gst-plugins-base
lib32-gst-plugins-base-libs
lib32-gst-plugins-good
lib32-gstreamer
```

Some 32-bit plugins don't exist in the official packages:

```sh
aur/lib32-gst-plugins-bad
aur/lib32-gst-plugins-bad-libs
aur/lib32-gst-plugins-ugly
aur/lib32-gst-libav
```

While installing AUR packages does work they have to be compiled and sometimes
are incompatible with the official packages. Gstreamer AUR Plugins have
improved a lot over time, but it's still possible that updates break them.

There are several methods to test if the video will work:

```sh
# Find the video and audio codecs
gst-discover-1.0 <video>
ffprobe <video>

# Play the video with 64-bit GStreamer
gst-play-1.0 <video>

# Play the video with 32-bit GStreamer
gst-play-1.0-32 <video>
```

If the video does not play you are probably missing a plugin.

When the video is available it's also possible to convert to another format:

```sh
# Legacy but may be the only option
ffmpeg -i input.mp4 -c:v libvpx-vp8 -crf 28 -c:a libvorbis output.webm

# Widely supported, good compression
ffmpeg -i input.mp4 -c:v libvpx-vp9 -crf 28 -c:a libopus output.webm

# Best compression and quality but slow to encode and less common
ffmpeg -i input.mp4 -c:v libaom-av1 -crf 28 -c:a libopus output.webm
```

A summary of popular video codecs:

| Codec | Release Year | Compression Efficiency       | Encoding Speed    | Hardware Support | Adoption       |
| ----- | ------------ | ---------------------------- | ----------------- | ---------------- | -------------- |
| VP8   | 2010         | Low                          | Fast              | Limited          | Legacy         |
| VP9   | 2013         | Better than VP8              | Slower than VP8   | Good             | Moderate       |
| AV1   | 2018         | Best among consumer codecs   | Slowest           | Least            | Emerging       |
| H.264 | 2009         | Moderate                     | Good              | Wide             | Ubiquitous     |
| H.265 | 2013         | Best among commercial codecs | Slower than H.264 | Very good        | Widely adopted |

A summary of popular audio codecs:

| Codec  | Release Year | Compression Efficiency | Audio Quality | Compatibility | Latency |
| ------ | ------------ | ---------------------- | ------------- | ------------- | ------- |
| MP3    | 1993         | Moderate               | Good          | Excellent     | Low     |
| Vorbis | 2000         | Good                   | High          | Moderate      | Medium  |
| Opus   | 2012         | Excellent              | Excellent     | Growing       | Low     |
| AAC    | 1997         | Excellent              | High          | Excellent     | Medium  |

However the engine must support one of these formats, and the script has to be
changed to load the converted file. Note that neither H.264 or H.265 are part
of `gst-plugins-good`, they are part of `gst-libav`, and so aren't going to
work with official packages in a 32-bit environment.

An alternative solution is to install Windows Media Player, to play WMV3
videos. To install DirectShow and Window Media Player 10 in a 32-bit prefix:

```bash
WINEPREFIX=$HOME/.wine32 WINEARCH=win32 winetricks directshow wmp10
```

The game then must be run with the same prefix:

```bash
WINEPREFIX=$HOME/.wine32 WINEARCH=win32 wine <exec>
```

### MIDI

MIDI was a popular format in older games but it needs to be configured manually
in Wine.

Install a soundfont and timidity++:

```sh
pacman -S freepats-general-midi timidity++
```

Add the soundfont to timidity configuration.

```sh
# /etc/timidity/timidity.cfg
soundfont /usr/share/soundfonts/freepats-general-midi.sf2
```

Add your user to the audio group:

```sh
sudo usermod -aG audio your_user_name
```

Restart the computer and check if the service is enabled:

```
systemctl status --user timidity.service
```

If it's not, start and enable it:

```
systemctl start --user timidity.service
systemctl enable --user timidity.service
```

To confirm that it's working first check if you have devices:

```
% aplaymidi -l
 Port    Client name                      Port name
 14:0    Midi Through                     Midi Through Port-0
128:0    TiMidity                         TiMidity port 0
128:1    TiMidity                         TiMidity port 1
128:2    TiMidity                         TiMidity port 2
128:3    TiMidity                         TiMidity port 3
```

Now that you know that you do, play a midi file:

```
aplaymidi your_midi_file.mid --port 128:0
```

There are other ways to have MIDI working but this is perhaps the easiest one,
detailed in Arch wiki.

## Game Engines

Each game engine has a dedicated page:

- [Pixel Game Maker MV](engines/PGMMV.md)
- [RPG Maker](engines/RPGMAKER.md)
- [ClickTeam](engines/CLICKTEAM.md)
- [Unity](engines/UNITY.md)

## Steam

For more information on [Steam](STEAM.md)

[Lutris.Gamescope]: screenshots/lutris_gamescope_options.webp
