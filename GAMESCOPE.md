# GameScope

Gamescope is a micro-compositor developed by Valve and specifically designed
for gaming. It can be used to enable features or fix some game problems.

<!-- mtoc-start -->

* [Installation](#installation)
* [Usage](#usage)
  * [Command Line](#command-line)
  * [Lutris](#lutris)
  * [Filters](#filters)
  * [Runtime Options](#runtime-options)
* [Examples](#examples)
  * [Classic Games](#classic-games)
* [Troubleshooting](#troubleshooting)
  * [Incorrect Resolution](#incorrect-resolution)
  * [Mouse Not Detected](#mouse-not-detected)

<!-- mtoc-end -->

## Installation

To install game-scope:

```sh
pacman -S gamescope
```

## Usage

### Command Line

To run a game with gamescope:

```sh
gamescope [options] -- <executable>
```

To see all options:

```sh
gamescope --help
```

The help options outputs to stderr so it might be useful to write the output to
a file:

```sh
gamescope --help &> gamescope-help.txt
```

Or if you don't want to write to a file first:

```sh
gamescope --help 2>&1 | less
```

### Lutris

After installing gamescope Lutris will have a Gamescope section:

![Lutris Gamescope](screenshots/lutris_gamescope.webp)

Advanced is need to configure gamescope options. Options not available in the
GUI can be set in the Custom Settings.

### Filters

One very useful option is filters:

```sh
gamescope -F [linear|nearest|fsr|nis|pixel] -- <executable>
```

All filters are performing upscaling, so 

Details of each filter:

- Linear: interpolates colors of the surrounding pixels. The end result is
  often blurry. It's the default upscaling method, even without gamescope.
- Nearest/Pixel: replaces every pixel with the nearest pixel in the ouput.
  Makes games look pixelated.
- FSR: AMD Fidelity FX Super Resolution. Performs spatial upscaling followed by
  sharpening to remove the blur.
- NIS: Nvidia Image Scaling. It is identical to FSR, but Nvidia version.

Here's how each filter looks on Devil of the Mirror on a 1440p monitor:

<table>
 <tr>
   <th>Linear</th>
   <th>Pixel</th>
   <th>FSR</th>
   <th>NIS</th>
 </tr>
 <tr>
   <td>
     <a href="screenshots/gamescope_linear_4by3.webp">
       <img src="screenshots/gamescope_linear_4by3_small.webp">
     </a>
   </td>
   <td>
     <a href="screenshots/gamescope_pixel_4by3.webp">
       <img src="screenshots/gamescope_pixel_4by3_small.webp">
     </a>
   </td>
   <td>
     <a href="screenshots/gamescope_fsr_4by3.webp">
       <img src="screenshots/gamescope_fsr_4by3_small.webp">
     </a>
   </td>
   <td>
     <a href="screenshots/gamescope_nis_4by3.webp">
       <img src="screenshots/gamescope_nis_4by3_small.webp">
     </a>
   </td>
 </tr>
</table>

### Runtime Options

Some options can be changed during run-time:

```
Super + F  toggle fullscreen
Super + N  toggle nearest neighbour filtering
Super + U  toggle FSR upscaling
Super + Y  toggle NIS upscaling
Super + I  increase FSR sharpness by 1
Super + O  decrease FSR sharpness by 1
Super + S  take a screenshot
Super + G  toggle keyboard grab
```

## Examples

### Classic Games

Modern computers are not designed to run classic games:

- Classic games were designed with 4:3 aspect ratio, not 16:9. This can result
  in the screen stretching in some games.
- Screens were CRT, not LCD, and resolutions were much lower (e.g., 320x240,
  640x480, 800x600, 1024x768). By default games with lower resolution in
  full-screen will use linear filtering, which may end up too blurry.
- Games were designed to run at best 60fps. Higher frame rates can make the
  game either run too fast, or result in tearing.

In order to fix all of these problems:

```sh
gamescope -S fit -F nearest -r 60
```

This will make the game keep aspect ratio, look pixelated, and be capped at
60fps.

## Troubleshooting

### Incorrect Resolution

Some games have a much lower resolution than the screen, and gamescope default
options don't set the desired resolution.

The simple fix is to set the resolution manually:

```
gamescope -w 1920 -h 1080
```

The two options set the game resolution. It's also possible to set the screen
resolution.

```
gamescope -W 2560 -H 1440 -w 1920 -h 1080
```

In the command above the screen is set to 1440p and the game to 1080p.

### Mouse Not Detected

Some games don't detect mouse hoovering or clicks with the default options. In
order to fix that problem:

```
gamescope --force-grab-mouse
```
