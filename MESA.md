# MESA

[MESA][mesa] is an open-source implementation of OpenGL and Vulkan.

## Environment Variables

There are several [environment variables][envvars] available in MESA.

The variables depend on the game and GPU. For example:

- RADV is the Vulkan driver for AMD GPUs
- RadeonSI is the OpenGL driver for AMD GPUs
- Zink is OpenGL implemented with Vulkan
- V3D is for Broadcom GPUs typically used in Raspberry Pi
- Freedreno is for Qualcomm GPUs

## HUD

MESA includes a HUD that is sometimes useful for debugging.

To get all options:

```
GALLIUM_HUD=help glxgears > HELP.txt
```

[mesa]: https://www.mesa3d.org
[envvars]: https://docs.mesa3d.org/envvars.html
