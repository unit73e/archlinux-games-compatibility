# Pixel Game Maker MV

[Pixel Game Maker MV (PGMMV)][PGMMV] allows to create 2D games without any need
of programming. It's often used in platform games.

<!-- mtoc-start -->

* [Technical Information](#technical-information)
* [Common Shortcuts](#common-shortcuts)
* [Troubleshooting](#troubleshooting)
  * [Japanese Games](#japanese-games)
  * [MESA glitches](#mesa-glitches)

<!-- mtoc-end -->

## Technical Information

A typical PGMMV will have the following file structure:

```sh
action_log.txt # main log file
player.exe     # default executable name but may have a different name
Resources/data # game information (e.g., decrypt key is here)
Resources/save # save files
Resources/*    # other game content (e.g., images, scripts)
plugins/*      # third-party plugins
```

Resources are encrypted so you can't just simply edit files as you wish,
including the save file, despite being in JSON. The decryption key is easy to
find, and the algorithm is XOR and TwoFish.

When the game is executed it will almost always show a PGMMV splash screen, so
games with this engine are usually easy to identify.

PGMMV typically uses the following framework/libraries:

- Cocos2D: a framework to develop 2D games.
- OpenAL: a cross-platform 3D audio API.
- GLEW: a cross-platform extension loading library.
- Iconv: a character encoding library.
- OpenSSL: a cryptography library used for secure communication protocols.
- Curl: a library for transferring data using various protocols.
- Mpg123: a library for decoding MPEG audio layer 1, 2, and 3.
- OGG: Ogg Vorbis audio codec library.
- VLC: VLC media player.
- MozJS: Mozilla JavaScript engine.
- SQLite: database engine library.
- UV: a cross-platform asynchronous I/O library.
- Websockets: web socket protocol implementation.
- Zlib: ZLib compression library.

Cocos2D is proprietary, but everything else is open-source, so in general games
run correctly.

## Common Shortcuts

PGMMV usually have predefined shortcuts:

- F1: shows menu bar.
- F5: restarts the game.
- Esc: toggles full-screen/window

## Troubleshooting

### Japanese Games

Some PGMMV have files with Japanese characters. The game will crash at start
when using a non-Japanese locale. To fix the problem:

```sh
LANG=ja_JP.UTF-8 wine player.exe
```

### MESA glitches

If a PGMMV game does not show graphics correctly (e.g., white rectangle) the
easy fix is to rename the executable to `player.exe`, which is the default
executable name of PGMMV games. The problem is described in full in the rest of
this section.

According to the OpenGL specification:

- OpenGL 2.0 supports GLSL 1.10.
- OpenGL 2.1 supports GLSL 1.20.

However PGMMV uses OpenGL 2.0 and GLSL 1.20. MESA by default does not allow
GLSL 1.20 on OpenGL 2.0. This might cause rendering glitches, such as a white
rectangle in the upper right corner of the screen.

Since MESA 23.1.1 this problem has been solved for all PGMMV games with a
`player.exe` executable. The fix is in `src/util/00-mesa-defaults.conf` of MESA
source code.

Another way is to add to `$HOME/.driconf`:

```bash
<driconf>
  <device screen="0">
    <application name="Pixel Game Maker MV" executable="player.exe">
      <option name="allow_glsl_120_subset_in_110" value="true" />
    </application>
  </device>
</driconf>
```

The configuration above overrides the default configuration, and more options
can be added.

Another safer way to write the DRI configuration file is to install `adriconf`
and follow these steps:

- Add a new profile or use an existing one.
- Go do the Debugging section.
- Enable "Allow a subset of GLSL 1.20 in GLSL 1.10 as needed by SPECviewperf13".

This will write a file similar to the one shown above.

One obvious problem with this solution is that all programs with `player.exe`
executable have GLSL 1.20 on OpenGL 2.0 enabled.

[PGMMV]:(https://rpgmakerofficial.com/product/act/en/index.html)
