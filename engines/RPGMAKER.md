# RPG Maker

RPG Maker is a series of programs to develop role playing games.

There are several versions, but in general they can be grouped:

- Legacy
  - RPG Tsukūru Dante 98
  - RPG Maker 95
  - RPG Maker 2000
  - RPG Maker 2003
- Ruby
  - RPG Maker XP
  - RPG Maker VX
  - RPG Maker VX Ace
- JavaScript
  - RPG Maker MV
  - RPG Maker MZ
- Unity
  - RPG Maker Unite

Depending on which group the game belongs, the technical details will be
different.

Table of contents:

<!-- mtoc-start -->

* [RPG Maker XP, VX, VX Ace](#rpg-maker-xp-vx-vx-ace)
  * [Missing Run Time](#missing-run-time)
* [RPG Maker MV and MZ](#rpg-maker-mv-and-mz)
  * [Default Keys](#default-keys)
  * [Full-screen](#full-screen)
  * [Black Screen](#black-screen)
    * [Defer Surface Creation](#defer-surface-creation)
    * [Override DLLs](#override-dlls)
    * [HTTP Server](#http-server)
      * [Fail to load files](#fail-to-load-files)
      * [Save Files](#save-files)
* [General](#general)
  * [Improve Image Quality](#improve-image-quality)
    * [Resolution](#resolution)
    * [Tearing](#tearing)

<!-- mtoc-end -->

## RPG Maker XP, VX, VX Ace

RPG Maker XP, VX and VX Ace are developed in Ruby.

The easiest way to identify these games is `Game.rgss*` exist. These are Ruby
Game Script System (RGSS) files:

- RPG Maker XP: `*.rgssad`.
- RPG Maker VX: `*.rgss2a`.
- RPG Maker VX Ace: `*.rgss3a`.

Another way is looking at the configuration file:

```
$ cat Game.ini
[Game]
Library=System\RGSS301.dll
Scripts=Data\Scripts.rvdata2
Title=My Game 1.0
```

Games in these versions typically have low resolutions, and end up being
stretched. To fix this problem see the [Improve Image
Quality](#improve-image-quality) section.

There's no hardware acceleration at all in XP, VX or VX Ace games. These games
run entirely on software using the CPU. If the game runs slow there's not much
that can be done to improve.

Translating is not easy. The RGSS file must be extracted, and that typically
results in `rvdata` files. These files are serialized Ruby code, and have
deserialized, translated, and serialized again.

### Missing Run Time

Some games don't include the run-time environment and will crash at start
asking to install it.

Go to the [RPGMaker](https://www.rpgmakerweb.com/run-time-package) page and
download and install the respective run time.

The game should work after installation.

## RPG Maker MV and MZ

RPG Maker MV and MZ are developed in JavaScript with the
[pixi.js](https://pixijs.com/) framework.

Games developed in this version can be identified by the `package.json` file:

```
cat package.json 
{
    "name": "MYGAME/RPGMV",
    "main": "www/index.html",
    "js-flags": "--expose-gc",
    "window": {
        "title": "",
        "toolbar": false,
        "width": 816,
        "height": 624,
        "icon": "www/icon/icon.png"
    }
}
```

Since it's JavaScript MV and MZ games run in a browser. The executable starts
an embedded chrome browser with OpenGL ES support. Since it's all in JavaScript
it's also possible to run MV and MZ games with a native browser, with some
caveats.

Translating is fairly easy since the data files are all in JSON. Data files are
usually in `www/data/*.json` and can be directly translated, but the game must
restart in order to take effect.

### Default Keys

RPG Maker MV and MZ usually default useful keys:

- F2, debug information
- F3, turns full-size on/off
- F4, turns full-screen on/off
- F5, resets the game

### Full-screen

Games run faster and more fluid when in full-screen exclusive mode. This is
because the compositor will only render the game, ignoring all windows.

Unfortunately RPG Maker MV games don't run in full-screen at start, so the only
two ways is changing the code, or pressing F4.

### Black Screen

Sometimes games use old OpenGL libraries and won't work with stock Wine. There
are a few solutions to this problem.

#### Defer Surface Creation

If the game starts with sound but there's no images the best way to solve it is
to use defer surface creation. This only works with DXVK.

Create a `dxvk.conf` file in the game folder:

```
dxgi.deferSurfaceCreation = True
d3d9.deferSurfaceCreation = True
```

Start the game and it should work.

#### Override DLLs

Another way is to override OpenGL DLLs.

```sh
WINEDLLOVERRIDES="libEGL,libGLESv2=b" wine <executable>
```

The command above uses built-in libraries instead of Windows native libraries.
One problem is the game will most likely run slowly.

#### HTTP Server

If overriding DLLs does not work it may be possible to run the game with an
HTTP server.

The `www` folder must exist.

Install the `nodejs-http-server` and run:

```sh
cd www
http-server
```

The game can be run by pressing the URL the server is running. There are some
potential problems with this solution, detailed next.

##### Fail to load files

RPG Maker games are developed for Windows and that means file-names are
cases-insensitive. Linux file-names, however, are case-sensitive, so it's
possible that some files will not load. This can be fixed by either fixing the
scripts or renaming files.

If an error like such happens when running the game:

```
Load Error <MyFile.png>
```

Check if the file has the same case:

```
grep -ri MyFile.png
```

If one of the data scripts shows as `myfile.png` it has to be changed to
`MyFile.png`. Either that or rename `MyFile.png` to `myfile.png`, or copy to
`myfile.png` so you have both versions.

##### Save Files

Save files will be stored in the browser cookies, so if you clean browser
cookies your save is lost.

Some games don't work very well if you have executed another RPGMaker game
before, using the cache of the previous game. In order to fix this clean the
browser cache. However note that you will also lose all saves.

One way to fix both problems is to run a dedicated chrome configuration:

```
chromium --user-data-dir=~/.rpgmaker/chromium-mygame
```

This will create a configuration per game. It is however hard to manage.

## General

### Improve Image Quality

Some games have two problems with the modern computers:

- The original aspect ratio is 4:3, not 16:9, so the image will stretch on
  full-screen and there's no option to keep aspect ratio.
- Most games have SD resolution. In most OS the default filtering when
  stretched is linear filtering, which makes the games look blurred.

Gamescope can fix these problems:

- `-S --scaler fit` will keep the aspect ratio.
- `-F --filter pixel` will make the game look pixelated.

To execute from the command line:

```sh
gamescope --S fit --F pixel -- wine <game.exe>
```

In RPG Maker MZ and MV when the game starts you may have to press F4 to switch
to full-screen. Otherwise the game will still look slightly blurred.

More information in [Gamescope](GAMESCOPE.md).

#### Resolution

Usually gamescope does a good job figuring out the game resolution, but if even
in full-screen the pixels don't look quite right, the resolution can be lowered
manually:

```sh
gamescope -w 800 -h 600 -F pixel -- wine <game.exe> 
```

Note that game resolution is not the same as output resolution. Gamescope will
stretch the game resolution to output resolution, and use a filter. In this
case pixel was chosen.

The default resolutions for each engine are:

- RPG Maker XP, VX, and VX Ace: internal 544x416
- RPG Maker MV and MZ: internal 816x624

These are non-standard resolutions and often changed by developers so the only
real way of knowing is figuring out manually.

The easiest and most precise way is to run the game in windowed mode, run
`xwininfo`, and click the window:

```
xwininfo: Please select the window about which you
          would like information by clicking the
          mouse in that window.

xwininfo: Window id: 0x1600005 "My Game"
  
  ...
  Width: 816
  Height: 624
  ...
```

So now the game can be executed with precise resolution:

```sh
gamescope -w 816 -h 624 -F pixel -- wine <game.exe>
```

#### Tearing

Sometimes RPG Maker games can have a tearing, especially with high-refresh rate
screens.

Default refresh rates of engines:

- RPG Maker XP: capped to 30 fps.
- RPG Maker VX and VX Ace: capped to 60fps.
- RPG Maker MV and MZ: default 60fps, but there's no cap.

In RPG Maker MV and MZ one way to avoid tearing is running the game in
full-screen mode by pressing F4. This will make the game run smoother thanks to
full-screen exclusive mode.

Another way is capping the frame-rate. Gamescope can mitigate that problem by
capping refresh rate:

```sh
gamescope -r 60 -- wine <game>
```

Another way is to change the monitor refresh rate manually.
