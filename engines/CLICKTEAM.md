# ClickTeam

[ClickTeam][clickteam] is a software company that makes game engines that don't
require scripting, the best known being ClickTeam Fusion. Usually games
developed with this engine run out of the box, but full-screen does not always
work.

## Fullscreen

This engine does not have full-screen mode by default. One way to have it
anyway is to spoof the resolution.

```bash
# The default options of gamescope should work
gamescope wine <executable>

# If not specify the resolution and refresh rate
gamescope -W 1920 -H 1080 -r 60 wine <executable>
```

## Options

All ClickTeam games have the following options:

```
/DIB => forces standard graphic mode
/DIB3 => forces standard graphic mode, 256 colors
/DIB4 => forces standard graphic mode, 16 millions of colors, 24 bits
/DIB6 => forces standard graphic mode, 32768 colors
/DIB7 => forces standard graphic mode, 65536 colors
/DIB8 => forces standard graphic mode, 16 millions of colors, 32 bits
/DEBUG => displays graphic mode in title bar
/MIS0 => disables the Machine Independent Speed option
/MIS1 => forces the Machine Independent Speed option
/NOF => runs in windowed mode
/NOC => prevents images from being compressed in memory (for internal test)
/NOX => disables Alt+F4
/NOK => disables the Keep Screen Ratio option (in full screen mode)
/VSYNC => forces V-Sync ON
```

This can help fix issues in some games.

[clickteam]: https://www.clickteam.com
