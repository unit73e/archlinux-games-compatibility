# Unity

Unity is a general purpose game engine. Games usually run out of the box with
Wine and one special feature is that auto-translation is supported.

<!-- mtoc-start -->

* [Auto-Translate](#auto-translate)
  * [IL2CPP](#il2cpp)
* [Troubleshooting](#troubleshooting)
  * [Input fails after unfocus](#input-fails-after-unfocus)

<!-- mtoc-end -->

## Auto-Translate

The best way to translate is to use [BepInEx][BepInEx] and
[XUnity.AutoTranslator][XUnity.AutoTranslator].

- Download `BepInEx` 32-bit and 64-bit 5.x latest version.
- Download `XUnity.AutoTranslator-BepInEx`.
- Check if the game is 32-bit or 64-bit by calling `file <executable>`.
- Extract BepInEx and XUnity-AutoTranslator in the root folder of the game.
- Run the game with `WINEDLLOVERRIDES="winhttp=n" wine <executable` or
  configure Lutris in the Runner Options tab, in the DLL overrides section.

### IL2CPP

Some games convert C# into C++ to run natively, for performance and
compatibility reasons. If that's the case files ending with `il2cpp` will be
found in the data folder of the game.

If the game is IL2CPP it will not work with BepInEx 5.x.

- Download:
  - `BepInEx` 32-bit and 64-bit 6.x latest version.
  - `XUnity.AutoTranslator-BepInEx`
  - `XUnity.AutoTranslator-BepInEx-IL2CPP`
- Check if the game is 32-bit or 64-bit by calling `file <executable>`.
- Extract all files in the root folder.
- Run the game with `WINEDLLOVERRIDES="winhttp=n" wine <executable` or
  configure Lutris in the Runner Options tab, in the DLL overrides section.

Note that for `XUnity.AutoTranslator` both `BepInEx` and `BepInEx-IL2CPP` must
be extracted. Otherwise only some text will be translated.

## Troubleshooting

### Input fails after unfocus

Unity games fail to focus after unfocus. To fix this problem:

```
winetricks usetakefocus=n
```

[BepInEx]: https://github.com/BepInEx/BepInEx
[XUnity.AutoTranslator]: https://github.com/bbepis/XUnity.AutoTranslator
