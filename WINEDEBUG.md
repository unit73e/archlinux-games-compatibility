# Wine Debug

Understanding why some game crashes is not an easy task because usually you
have a binary, not source code, so you have to guess what HEX memory location
is trying to do. Nevertheless there are ways to help you understand.

## Wine Debug Channels

The first way is to use debug channels and there are several, but the most
useful if probably relay.

```sh
WINEDEBUG=+relay wine game.exe &> output.txt
```

This will output all calls that cross the DLL boundary. If you get an exception
usually it will say something like:

```
wine: Unhandled exception ...
```

And there are codes for some well known exceptions:

- `0xc0000005` is a segmentation fault.
- `0xeedfade` is a Delphi exception
- `0xe06d7363` is a Microsoft Visual C++ exception

Right before the exception the `+relay` flag should give an hint of what it was
doing in that particular thread.

More information here:
- https://wiki.winehq.org/Debug_Channels
- https://wiki.winehq.org/Debugging_Hints

## Libraries

One way to figure why a game is crashing is checking which libraries it's
trying to import and in linux that would be `readelf` or `ldd` but that's for
ELF files, not PE files.

One say is to use Wine itself:

```
WINEDEBUG=+loaddll wine game.exe &> output.txt
```

Another is to use a specialized tool:

```
paru -Syu readpe
```

This allows to check all information about the file with:

```
readpe game.exe
```

With that you may be able to figure out what the problem is.

## Wine Debugger

Another way is to use wine debug utility. There are several ways to use this
tool but the easiest is to first run the program.

```
wine game.exe
```

And then use wine debug in a separate window:

```
winedbg
```

Now continue the application until it crashed and close the crash report
window. On the wine debug window get the back-trace of all processes:

```
> bt all
```

Find were your process is, and check the back-trace. That will give you more
information than the crash dialog report, but by default you only really get
memory locations, not source code.

To have debug symbols first add the multilib debug repository:

```
# /etc/pacman.conf
[multilib-debug]
Include = /etc/pacman.d/mirrorlist
```

And install wine debug:

```
pacman -Sy wine-debug
```

That should give you a bit more information where the crash occurred.

It's also possible to attach to a specific process to get only the back-trace
of a particular application, but it doesn't always work, so back-tracing
everything is the safest bet.

More information here: https://wiki.winehq.org/Winedbg
